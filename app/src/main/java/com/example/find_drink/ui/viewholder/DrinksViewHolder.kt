package com.example.find_drink.ui.viewholder

import android.util.SparseBooleanArray
import androidx.core.util.isNotEmpty
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.find_drink.databinding.DrinksViewBinding
import com.example.find_drink.models.Drinks
import com.example.find_drink.room.entities.FavouriteEntity

class DrinksViewHolder(val binding: DrinksViewBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(
        drinks: Drinks,
        favlist: List<FavouriteEntity>,
        favourite_items: SparseBooleanArray,
        position: Int
    ) {
        binding?.apply {
            Glide.with(binding.root.context).load(drinks.strDrinkThumb).circleCrop().into(thumb)
            title.text = drinks.strDrink
            alcholic.isChecked = drinks.strAlcoholic == "Alcoholic"

            favlist.forEach {
                if (it.drinkId == drinks.idDrink?.toInt()) {
                    favourite_items.put(position, true)
                } /*else {
                    if (favourite_items.isNotEmpty()) {
                        favourite_items.delete(position)
                    }
                }*/

            }

        }
    }
}