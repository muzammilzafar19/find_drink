package com.example.find_drink.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import java.lang.Exception
import javax.inject.Inject

class ConnectionDetector @Inject constructor (val context: Context) {

    fun isNotConnectedToInternet(): Boolean {
        var connectivity: ConnectivityManager? = null
        try {
            connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (connectivity != null) {
            val info = connectivity.allNetworkInfo
            for (networkInfo in info) if (networkInfo.state == NetworkInfo.State.CONNECTED) {
                return false
            }
        }
        return true
    }
}