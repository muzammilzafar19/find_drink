package com.example.find_drink.di

import android.content.Context
import androidx.room.Room
import com.example.find_drink.room.dao.FarvouriteDao
import com.example.find_drink.room.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Singleton
    @Provides
    fun provideRoomDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "Drinks.db")
            .build()
    }

    @Provides
    fun provideClientDao(appDatabase: AppDatabase): FarvouriteDao {
        return appDatabase.farvouriteDao()
    }
}