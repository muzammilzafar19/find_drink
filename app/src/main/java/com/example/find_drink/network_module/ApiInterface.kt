package com.example.find_drink.network_module

import com.example.find_drink.models.Drinks
import com.example.find_drink.models.ResponseModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("search.php?s=")
    suspend fun getDrinksByName(@Query("s") name :  String ) : ResponseModel

    @GET("search.php?f=")
    suspend fun getDrinksByAlphabet(@Query("f") name :  String ) : ResponseModel
}