package com.example.find_drink.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.find_drink.models.Drinks
import com.example.find_drink.repository.MainRepo
import com.example.find_drink.room.entities.FavouriteEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor (val mainRepo: MainRepo) : ViewModel() {

   fun getAllDrinkByNameLD(name : String) :LiveData<List<Drinks>> = mainRepo.getAllDrinksByName(name)
   fun getAllDrinkByAlPhabetLD(name : String) :LiveData<List<Drinks>> = mainRepo.getAllDrinksByALpha(name)
   fun getAllFavouriteLD() :LiveData<List<FavouriteEntity>> = mainRepo.getFavourite()
   suspend fun getSpecificFavourite(drinkid: String ) :Int  =  mainRepo.getSpecificFavourite(drinkid.toInt())
  lateinit var callback : (flag : Boolean)->Unit
   fun setRemoveAddCallback(callback : (flag : Boolean)->Unit)
   {
      this.callback=callback
   }

   fun addFavourite(drinks: Drinks?)
   {
      val favouriteEntity=FavouriteEntity()
      drinks?.let {
         favouriteEntity.drinkName=it.strDrink
         favouriteEntity.alcholic= it.strAlcoholic
         favouriteEntity.thumnail=it.strDrinkThumb
         favouriteEntity.date=it.dateModified.toString()
         favouriteEntity.drinkId=it.idDrink?.toInt()!!
         mainRepo.addFavourite(favouriteEntity)
      }

   }

   fun removeFavourite(drinkid : String)
   {
      val id = drinkid.toInt()
      mainRepo.removeFavourite(id)

   }

   fun addremoveFavourite(drinks: Drinks)
   {
      viewModelScope.launch {

         if(mainRepo.farvouriteDao.getSpecificFavourite(drinks.idDrink?.toInt()!!)==0)
         {
            addFavourite(drinks)
            callback(true)
         }
         else{
            removeFavourite(drinks.idDrink!!)
            callback(false)


         }
      }

   }





}