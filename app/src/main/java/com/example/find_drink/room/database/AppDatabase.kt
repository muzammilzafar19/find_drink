package com.example.find_drink.room.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.find_drink.room.dao.FarvouriteDao
import com.example.find_drink.room.entities.FavouriteEntity

@Database(
    entities = [
        FavouriteEntity::class,

    ],
    version = 1
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun farvouriteDao(): FarvouriteDao

}