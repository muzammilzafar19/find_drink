package com.example.find_drink.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.find_drink.room.entities.FavouriteEntity

@Dao
interface FarvouriteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavourite(favouriteEntity: FavouriteEntity)

    @Query("delete from FavouriteEntity where drinkId=:drinkId")
    suspend fun removeFavourite(drinkId : Int)

    @Query("select  count(*) from FavouriteEntity where drinkId=:drinkId")
    suspend fun getSpecificFavourite(drinkId : Int) : Int

    @Query("select  * from FavouriteEntity")
    suspend fun getSpecificFavourite() : List<FavouriteEntity>

    @Query("SELECT * FROM FavouriteEntity ORDER BY RANDOM() LIMIT 1")
    suspend fun getrandomFavourite() : FavouriteEntity



}