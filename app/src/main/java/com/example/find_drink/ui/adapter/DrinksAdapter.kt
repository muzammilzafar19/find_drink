package com.example.find_drink.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.find_drink.R
import com.example.find_drink.databinding.DrinksViewBinding
import com.example.find_drink.models.Drinks
import com.example.find_drink.room.entities.FavouriteEntity
import com.example.find_drink.ui.viewholder.DrinksViewHolder

class DrinksAdapter(val list: List<Drinks>,val favlist : List<FavouriteEntity>, val callBack: (position: Int) -> Unit) :
    RecyclerView.Adapter<DrinksViewHolder>() {
    private var favourite_items: SparseBooleanArray = SparseBooleanArray()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        return DrinksViewHolder(
            DrinksViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        val drinks = list[position]
        holder.bind(drinks,favlist,favourite_items,position)
        holder.binding.favorite.setOnClickListener {
            callBack(position)
        }
        toggalSelector(position,holder,holder.itemView.context)


    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun toggalSelector(
        position: Int,
        holder: DrinksViewHolder,
        context: Context
    ) {
        if (favourite_items.get(position, false)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                holder.binding.favorite.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_baseline_star_24
                    )
                )

            } else

                holder.binding.favorite.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_baseline_star_24
                    )
                )

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.binding.favorite.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_baseline_star_empty
                    )
                )
            } else {
                holder.binding.favorite.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_baseline_star_empty
                    )
                )

            }
        }
    }



    fun toggleSelection(pos: Int) {
        if (favourite_items[pos, false]) {
            favourite_items.delete(pos)

        } else {
            favourite_items.put(pos, true)


        }
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clearSelections() {
        favourite_items.clear()
        notifyDataSetChanged()
    }

    fun getSelectedItemCount(): Int {

        return favourite_items.size()
    }

    fun getSelectedList(): SparseBooleanArray {
        return favourite_items
    }
}