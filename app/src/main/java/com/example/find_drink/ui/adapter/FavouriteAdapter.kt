package com.example.find_drink.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.find_drink.databinding.FavouriteListViewBinding
import com.example.find_drink.room.entities.FavouriteEntity
import com.example.find_drink.ui.viewholder.FavouriteViewHolder

class FavouriteAdapter(val fav : List<FavouriteEntity>, val callback : (position : Int)->Unit) : RecyclerView.Adapter<FavouriteViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteViewHolder {
        return FavouriteViewHolder(FavouriteListViewBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: FavouriteViewHolder, position: Int) {
        holder.bind(fav[position])
        holder.binding.favorite.setOnClickListener {
            callback(position)
        }
    }

    override fun getItemCount(): Int {
        return fav.size
    }
}